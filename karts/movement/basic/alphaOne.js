const AlphaOne = class extends Kart {
    // Don't remove these 2 lines. The channel is required for the communication with the game.
    constructor(channel) {
        super(channel);

        // Place to store variables. For example: this.colorScheme = {}
        this.doNextThing = false;

        this.setColors(0x0000FF, 0x111111, 0xFFFFFF, 0x0000FF, 0x0000FF, 0xFFFFFF);
    };

    // Run is different from RoboCode.
    // Kart class: Run gets called continuesly and updates the queue in real time.
    // AdvancedKart class: Run gets called every tick (like the while loop in RoboCode)
    run() {
        // Kart class: The moves here are excecuted in the order they are given.
        this.ahead(100);
        this.turnRight(91);
        this.back(97);
        this.turnLeft(91);
        this.ahead(100);
        this.turnLeft(91);
        this.ahead(100);
        this.turnLeft(91);
    };
}
