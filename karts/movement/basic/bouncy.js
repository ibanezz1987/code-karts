const Bouncy = class extends Kart {
    // Don't remove these 2 lines. The channel is required for the communication with the game.
    constructor(channel) {
        super(channel);

        // Place to store variables. For example: this.colorScheme = {}
        this.doNextThing = false;

        this.setColors(0x0ff000, 0x111111, 0x000000, 0x0ff000, 0x0ff000, 0x000000);
    };

    onHitWall(event) {
        this.turnLeft(event.getBearing());
        this.ahead(10);
    };

    // Kart class: Run gets called continuesly and updates the queue in real time.
    // AdvancedKart class: Run gets called every tick
    run() {
        // Kart class: The moves here are excecuted in the order they are given.
        this.ahead(80);
        this.turnRight(1);
    };
}
