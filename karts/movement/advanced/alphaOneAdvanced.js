const AlphaOneAdvanced = class extends AdvancedKart {
    // Don't remove these 2 lines. The channel is required for the communication with the game.
    constructor(channel) {
        super(channel);

        // Place to store variables. For example: this.colorScheme = {}
        this.inversed = false;

        this.setColors(0xFF0000, 0xFF0000, 0x111111, 0xFFFFFF, 0x111111, 0x111111);
    };

    // Run is different from RoboCode
    // AdvancedKart class: Run gets called every tick (like the while loop in RoboCode)
    run() {
        this.setAhead(10);

        if (this.getHeading() <= 6 && !this.inversed) {
            this.setTurnLeft(10);
            this.inversed = true;
        }
        else if (this.inversed) {
            this.setTurnRight(10);

            if (this.getHeading() >= 354) {
                this.inversed = false;
            }
        }
        else {
            this.setTurnLeft(10);
        }


        this.execute();
    };
}
