const CheckPointScanner = class extends AdvancedKart {
    // Don't remove these 2 lines. The channel is required for the communication with the game.
    constructor(channel) {
        super(channel);

        // Place to store variables. For example: this.colorScheme = {}
        this.isSearching = true;
        this.currentCheckPoint = 0;

        this.setColors(0x00ff00, 0xffff00, 0xffffff, 0x111111, 0x111111, 0x111111);
    };

    // Run is different from RoboCode
    // AdvancedKart class: Run gets called every tick (like the while loop in RoboCode)
    run() {
        if(this.isSearching) {
            this.setTurnLeft(10);
        } else {
            this.setAhead(30);
        };
        this.execute();
    };

    onScannedCheckpoint(event) {
        if(event.index === this.currentCheckPoint) {
            this.isSearching = false;
        };
    };

    onReachCheckpoint(event) {
        if(event.index === this.currentCheckPoint) {
            this.isSearching = true;
            this.currentCheckPoint++;

            if(this.currentCheckPoint === this.getTotalCheckpoints()) {
                this.currentCheckPoint = 0;
            };
        };
    };
}