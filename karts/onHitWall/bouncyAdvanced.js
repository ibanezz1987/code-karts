const BouncyAdvanced = class extends AdvancedKart {
    // Don't remove these 2 lines. The channel is required for the communication with the game.
    constructor(channel) {
        super(channel);

        this.hittingWall = false;
        this.hittingWallBearing = 0;

        // Place to store variables. For example: this.colorScheme = {}

        this.setColors(0x111111, 0xff0000, 0x111111, 0x333333, 0x111111, 0x111111);
    };

    onHitWall(event) {
        this.hittingWall = true;
        this.hittingWallBearing = event.getBearing();
    };

    // Run is different from RoboCode
    // AdvancedKart class: Run gets called every tick (like the while loop in RoboCode)
    run() {
        if (this.hittingWall && (this.hittingWallBearing > 120 || this.hittingWallBearing < -120)) {
            this.hittingWall = false;
            this.setAhead(10);
        }
        else if (this.hittingWall) {
            this.setTurnRight(10);
        }
        else {
            this.setAhead(1000);
        };

        this.execute();
    };
}
