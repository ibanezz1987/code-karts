const Spinner = class extends AdvancedKart {
    // Don't remove these 2 lines. The channel is required for the communication with the game.
    constructor(channel) {
        super(channel);

        // Place to store variables. For example: this.colorScheme = {}
        this.inversed = false;
        this.isHittingKart = false;
        this.headingWhenHitting = null;

        this.setColors(0xaa00aa, 0x333333, 0x333333, 0xaa00aa, 0x333333, 0x333333);
    };

    // Run is different from RoboCode
    // AdvancedKart class: Run gets called every tick (like the while loop in RoboCode)
    run() {
        this.setAhead(10);

        if (this.isHittingKart) {
            if (this.getHeading < this.headingWhenHitting - 180 || this.getHeading > this.headingWhenHitting + 180) {
                this.isHittingKart = false;
            }
        }

        if (this.inversed) {
            this.setTurnRight(10);
        }
        else {
            this.setTurnLeft(10);
        }


        this.execute();
    };

    // TODO: complete onHitOpponent event
    onHitOpponent(event) {
        console.log(event);

        // TODO: concept doesn't work because its not based on
        //       the direction of the collision (instead on the kart itself)
        if (!this.isHittingKart) {
            this.inversed = !this.inversed;
            this.isHittingKart = true;
            this.headingWhenHitting = this.getHeading();
        }
    };
}
