const Panda = class extends AdvancedKart {
    // Don't remove these 2 lines. The channel is required for the communication with the game.
    constructor(channel) {
        super(channel);

        // Place to store variables. For example: this.colorScheme = {}

        this.setColors(0x111111, 0xffffff, 0x111111, 0xffffff, 0x111111, 0xffffff);
    };
}
