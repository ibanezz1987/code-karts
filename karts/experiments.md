Wat doet robocode bij een AdvancedRobot?

- Hoe ziet de queue eruit?
- - Staan turns los van ahead/back? (2 losse queues)
        Yes!
- - Zijn ahead/back blokking actions?
        Nope!
- - Zijn turns blokking actions?
        Nope!
- - Worden de acties allemaal direct uitgevoerd?
        YES! Alles tegelijkertijd obv de call execute(); !

- Wat gebeurt er bij 2x execute() in run functie?
- - Worden er 2 queues achter elkaar uitgevoerd?
        Nope!
- - Wordt alleen de 1e uitgevoerd?
        Nope!
- - Wordt alleen de 2e uitgevoerd?
        Nope!
- - Wordt er een error gegeven?
        Nope!
        De robot twitched tussen de beginacties van beide queues.

- Wat gebeurt er bij setAhead(600) + setTurnLeft(1)?
- - Na een tick draaien in een rechte lijn
        Nope!
- - Rijdt in grote cirkels
        Bingo!

- Hoe werken if statements binnen een mode als "run()"?
- - Ze worden na iedere keer doorlopen van queue uitgevoerd.
        Nope!
- - Ze worden pas uitgevoerd nadat er van mode is gewisseld.
        Nope!
        Ze worden bij start al uitgevoerd!
        (if wordt na eerste execute() omgezet)
        Wordt dit al bereikt voordat de queue start? Wellicht!
        Bovendien wordt een tegengestelde actie overruled door laatste actie uit queue.

- Hoe wordt de lengte van een andere mode dan run() bepaald?
- - Alle code wordt uitgevoerd
- - - Vervolgens wacht het spel tot de laatste move klaar is.
        Nope! Enkel het moment dat het event plaats vindt wordt de queue gebruikt.
- Hoe kun je dan naar een robot toe draaien?
- - Verschillende manieren
        Door ipv robot zelf de gun te draaien
        Door variabele te zetten die tijdens de run() while loop wordt uitgevoerd.

- Werken ifs echt alleen maar 1 keer per mode?
- - Ja, in ieder geval voor andere modes dan run()
- En bij run()?
- - De while loop zorgt ervoor dat deze continue wordt ververst.

Conclusie:

- execute() creert een nieuw queue element.
- Acties zonder "set" zoals turnLeft() maken een eigen queue element!
- Iedere queue wordt uitgevoerd tijdens een specifieke mode als "run()"
- Turns staan los van ahead/back.
- Code binnen een mode wordt in een keer uitgevoerd!
- Een tegenovergestelde actie in een queue wordt overschreven door de laatste actie.
- - setBack(100); setLeft(90); setAhead(150); setRight(45); execute();
- - Wordt: setAhead(150); setRight(45); execute();
- Timeouts gaan nooit werken.
- De while loop in run() zorgt ervoor dat de code daar bij iedere tick wordt aangeroepen.
- Andere modes dan run() duren 1 tick. De queue van deze modes duren dan enkel 1 tick.

====

Hoe moet een Robot/AdvancedRobot dan geschreven worden?

Tijdens iedere tick moet opnieuw worden aangeroepen wat er moet gebeuren.
Hier dient de while loop voor.
De while loop wordt altijd bij een nieuwe tick aangeroepen.

Robot class werkt met een queue voor iedere actie.
AdvancedRobot class werkt met een of meer paralelle acties die door execute 1x wordt aangeroepen.
Deze queue bevat max 1 element voor turns en 1 voor acceleration (ahead/back) + enkele andere sets


De "niet-set* moves" (ahead(), turnRight() etc) voeren zichzelf direct uit en kunnen pas weer worden aangeroepen als ze afgerond of onderbroken zijn.
https://robocode.sourceforge.io/docs/robocode.dotnet/html/42edc934-b4e1-0785-a8cb-5aecbc876d9b.htm
