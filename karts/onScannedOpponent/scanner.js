const Scanner = class extends AdvancedKart {
    // Don't remove these 2 lines. The channel is required for the communication with the game.
    constructor(channel) {
        super(channel);

        // Place to store variables. For example: this.colorScheme = {}
        this.isSearching = true;

        this.setColors(0x0000ff, 0x00ff00, 0xffffff, 0x111111, 0x111111, 0x111111);
    };

    // Run is different from RoboCode
    // AdvancedKart class: Run gets called every tick (like the while loop in RoboCode)
    run() {
        if(this.isSearching) {
            this.setTurnLeft(10);
            this.execute();
        };
    };

    onScannedOpponent(event) {
        // TODO: event
        // console.log(event);

        this.isSearching = false;
    }
}
