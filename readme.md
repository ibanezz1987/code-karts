# RoboRace

### Concept

Programming game zoals RoboCode. Racers moeten van punt naar punt om energie te behouden. Als de energie op is gaan ze terug naar het laatste punt dat ze hebben bezocht. Zodoende is er ruimte voor strategie.

Daarnaast kunnen Racers kogels afschieten. Dit kost veel energie, dus hier moeten ze strategisch mee om gaan.

### Overeenkomsten RoboCode

In JavaScript worden 'Robot Racers' geprogrammeerd. Robots kunnen kogels afschieten, met een bepaalde snelheid voortbewegen en bochten nemen.

Als zintuig heeft de kart een radar die hem informatie geeft over het dichtstbijzijnde object in de lijn waarop hij gericht staat. Objecten die de radar kan scannen zijn de punten (die de track vormen), kogels en Robots. Informatie die de radar opdoet is de coordinaat en het type object.
