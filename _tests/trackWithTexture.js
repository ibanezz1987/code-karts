// https://pixijs.io/examples/#/mesh-and-shaders/textured-mesh-basic.js

const app = new PIXI.Application();
document.body.appendChild(app.view);

let count = 0;

// build a rope!
const ropeLength = 918 / 20;

// const points = [];

// for (let i = 0; i < 20; i++) {
//    points.push(new PIXI.Point(i * ropeLength, 0));
//}

const points = [
    new PIXI.Point(250, 50),
    new PIXI.Point(200, 50),
    new PIXI.Point(150, 50),
    new PIXI.Point(100, 50),
    new PIXI.Point(50, 100),
    new PIXI.Point(100, 200),
    new PIXI.Point(50, 300),
    new PIXI.Point(70, 380),
    new PIXI.Point(130, 430),
    new PIXI.Point(250, 450),
    new PIXI.Point(300, 500),
    new PIXI.Point(400, 500),
    new PIXI.Point(450, 400),
    new PIXI.Point(475, 325),
    new PIXI.Point(440, 270),
    new PIXI.Point(390, 270),
    new PIXI.Point(300, 250),
    new PIXI.Point(280, 170),
    new PIXI.Point(300, 100),
    new PIXI.Point(250, 50),
    new PIXI.Point(200, 50),
];

const strip = new PIXI.SimpleRope(PIXI.Texture.from('examples/assets/snake.png'), points);

strip.x = -459;

const snakeContainer = new PIXI.Container();
snakeContainer.x = 400;
snakeContainer.y = 300;

snakeContainer.scale.set(800 / 1100);
app.stage.addChild(snakeContainer);

snakeContainer.addChild(strip);
