const Track = class {
    constructor() {
        this.points = [];
    };

    getTrack() {
        if(this.points.length === 0) {
            this.getDefaultCheckPoints();
        }

        return this.points;
    };

    getDefaultCheckPoints() {
        this.points = [
            { x: 250, y: 50,  radius: 3, index: 0 },
            { x: 200, y: 50,  radius: 3, index: 1 },
            { x: 150, y: 50,  radius: 3, index: 2 },
            { x: 100, y: 50,  radius: 3, index: 3 },
            { x: 50,  y: 100, radius: 3, index: 4 },
            { x: 100, y: 200, radius: 3, index: 5 },
            { x: 50,  y: 300, radius: 3, index: 6 },
            { x: 70,  y: 380, radius: 3, index: 7 },
            { x: 130, y: 430, radius: 3, index: 8 },
            { x: 250, y: 450, radius: 3, index: 9 },
            { x: 300, y: 500, radius: 3, index: 10 },
            { x: 400, y: 500, radius: 3, index: 11 },
            { x: 450, y: 400, radius: 3, index: 12 },
            { x: 475, y: 325, radius: 3, index: 13 },
            { x: 440, y: 270, radius: 3, index: 14 },
            { x: 390, y: 270, radius: 3, index: 15 },
            { x: 300, y: 250, radius: 3, index: 16 },
            { x: 280, y: 170, radius: 3, index: 17 },
            { x: 300, y: 100, radius: 3, index: 18 },
        ];
    };

    setTrack(track) {
        if(track.length > 1) {
            this.points = track;
        }
        else {
            // TODO: display notification that a track has a minimum length
        }
    };
}

