const TrackEditor = class {
    constructor(renderer, canvas) {
        this.renderer = renderer;
        this.canvas = canvas;

        this.isActive = true;

        this.track = [];

        this.addCanvasEvents();
    };

    addCanvasEvents() {
        this.canvas.addEventListener("click", function(event) {
            if(this.isActive) {
                this.updatePoint(event.clientX, event.clientY);
            };
        }.bind(this), false);

        this.canvas.addEventListener("touchend", function(event) {
            if(this.isActive && event.changedTouches.length) {
                this.updatePoint(event.changedTouches[0].clientX, event.changedTouches[0].clientY);
            };
        }.bind(this), false);
    };

    updatePoint(x, y) {
        this.track.push({ x: x, y: y, radius: 3, index: this.track.length, type: 'checkpoint' })

        this.redrawTrack();
    };

    redrawTrack() {
        this.renderer.resetStage();
        this.renderer.drawStageBorders();
        this.renderer.drawTrack(this.track);
    }

    getTrack() {
        return this.track;
    };

    quit() {
        this.track = [];
        this.isActive = false;
    };

    start() {
        this.isActive = true;
    };
}