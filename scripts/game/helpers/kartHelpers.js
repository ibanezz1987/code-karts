const KartHelpers = class {
    constructor(kartPeers) {
        this.karts = kartPeers;
    };

    getOtherKarts(currentKart) {
        let otherKarts = [];

        this.karts.forEach(kart => {
            if (kart.id !== currentKart.id) {
                otherKarts.push(kart)
            }
        });

        return otherKarts;
    };
};