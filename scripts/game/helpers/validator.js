const Validator = class {
    constructor() {
        this.disableBlockingJSFunctions();
    };

    disableBlockingJSFunctions() {
        alert = prompt = confirm = this.overrideFunction;
    };

    overrideFunction() {
        console.info('Function unavailable');
    };

    validate(codeSnippet) {
        const invalidArray = ['window', 'document', 'location', 'parent', 'PIXI', 'focus', 'frame', 'menubar', 'navigator', 'resize', 'scroll', 'cookie', 'Tools'];
        const stringClass = codeSnippet + '';
        let isValid = true;
        let foundInvalidValue = '';

        invalidArray.forEach(invalidValue => {
            if (stringClass.indexOf(invalidValue) !== -1) {
                isValid = false;
                foundInvalidValue = invalidValue;
            };
        });

        return { isValid: isValid, invalidValue: foundInvalidValue };
    };
};
