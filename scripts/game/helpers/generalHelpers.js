const GeneralHelpers = class {
    normalRelativeAngle(angle) {
        if (angle > -180 && angle <= 180) {
            return angle;
        };

        let fixedAngle = angle;

        while (fixedAngle <= -180) {
            fixedAngle += 360;
        };

        while (fixedAngle > 180) {
            fixedAngle -= 360;
        };

        return fixedAngle;
    };

    normalAbsoluteAngle(angle) {
        if (angle >= 0 && angle < 360) {
            return angle;
        }

        let fixedAngle = angle;

        while (fixedAngle < 0) {
            fixedAngle += 360;
        };

        while (fixedAngle >= 360) {
            fixedAngle -= 360;
        };

        return fixedAngle;
    };

    degreesToRadians(degrees) {
        return degrees * Math.PI / 180;
    };

    radiansToDegrees(radians) {
        return 180 / Math.PI * radians;
    };
};
