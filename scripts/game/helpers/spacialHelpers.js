const SpacialHelpers = class {
    // Check if line intersects circle (used for lasers detection)
    isLineIntersectingCircle(aX, aY, bX, bY, cX, cY, radius) {
        // compute the euclidean distance between A and B
        const LAB = Math.sqrt( Math.pow(bX - aX, 2) + Math.pow(bY - aY, 2) );

        // compute the direction vector D from A to B
        const Dx = (bX-aX)/LAB;
        const Dy = (bY-aY)/LAB;

        // the equation of the line AB is x = Dx*t + Ax, y = Dy*t + Ay with 0 <= t <= LAB.
        // compute the distance between the points A and E, where
        // E is the point of AB closest the circle center (Cx, Cy)
        const t = Dx*(cX-aX) + Dy*(cY-aY);

        // compute the coordinates of the point E
        const Ex = t*Dx+aX;
        const Ey = t*Dy+aY;

        // compute the euclidean distance between E and C
        const LEC = Math.sqrt( Math.pow(Ex-cX, 2) + Math.pow(Ey-cY, 2) );

        // test if the line intersects the circle, added extra checks to make sure its not the opposite direction.
        if(LEC <= radius && (aX < cX && bX > cX || aX > cX && bX < cX) && (aY < cY && bY > cY || aY > cY && bY < cY)) {
            return true;
        }

        return false;
    }

    // Check if 2 circles intersect
    circleIntersect(x1, y1, r1, x2, y2, r2) {
        // Calculate the distance between the two circles
        var squareDistance = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);

        // When the distance is smaller or equal to the sum
        // of the two radius, the circles touch or overlap
        return squareDistance <= ((r1 + r2) * (r1 + r2));
    };
}