const BattleSettings = class {
    constructor() {
        // General
        this.stage = {
            width: window.innerWidth,
            height: window.innerHeight,
        };
        this.frameRate = 60;
        this.isPaused = false;

        // Karts
        this.karts = [
            { name: 'AlphaOne', class: AlphaOne },
            { name: 'AlphaOneAdvanced', class: AlphaOneAdvanced },
            { name: 'Bouncy', class: Bouncy },
            { name: 'BouncyAdvanced', class: BouncyAdvanced },
            { name: 'Spinner', class: Spinner },
            { name: 'Panda', class: Panda },
            { name: 'Scanner', class: Scanner },
            { name: 'CheckPointScanner', class: CheckPointScanner },
        ];
        // this.selectedKarts = [0, 1, 2, 3, 4, 5, 6, 7];
        this.selectedKarts = [7];
    };

    getValue(value) {
        return this[value];
    };
};
