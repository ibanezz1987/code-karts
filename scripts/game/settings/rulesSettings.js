const RulesSettings = class {
    constructor() {
        this.seasonRules = {
            playSeason: false,
            tracks: [],
        };

        this.defaultGameRules = {
            quickSimulation: false,         // Test mode
            disableFrameRateChanges: false,
            enableCollisions: true,
            conceilEnemyHealth: false,      // Anti wave surfer
            bulletsVisible: false,          // Pro wave surfer
            infiniteHealth: true,
            autoDecreaseEnergy: true,
            ammunitionPickups: false,       // Arcade racer experimental gameplay
            traps: false,                   // Arcade racer experimental gameplay
            robocodeBattleMode: false,      // Empty stage with RoboCode like gameplay
            allPointRequired: false,
            allPointsInOrder: false,
            eliminationEachRound: false,
            enableCatchUp: false,
            captureTheChao: false,
            totalDarkness: false,           // Karts can't scan points/karts, they only notice contact
            editRules: false,
            numberOfRounds: 5,
            qualificationLap: false,
        };

        gameModeList = ['race', 'elimination', 'battleRace', 'arcadeRacer', 'sticktRacing', 'captureTheChao', 'customRules',];

        gameModes = {
            race: {
                name: 'Race',
                rules: {},
            },
            elimination: {
                name: 'Elimination',
                rules: {
                    eliminationEachRound: true,
                },
            },
            battle: {
                name: 'Battle Race',
                rules: {
                    infiniteHealth: false,
                    enableCatchUp: true,
                },
            },
            battle: {
                name: 'Arcade racer',
                rules: {
                    enableCatchUp: true,
                    ammunitionPickups: false,
                    traps: false,
                },
            },
            sticktRacing: {
                name: 'Stickt racing',
                rules: {
                    qualificationLap: true,
                    allPointRequired: true,
                    allPointsInOrder: true,
                },
            },
            captureTheChao: {
                name: 'Capture the chao',
                rules: {
                    captureTheChao: true,
                    enableCatchUp: false,
                },
            },
            customRules: {
                name: 'Custom rules',
                rules: {
                    editRules: true,
                },
            },
        };
    };

    getRules(mode) {
        let defaultRules = this.defaultGameRules,
            selectedRules = this.gameModes[mode].rules,
            modeRules = Object.assign(defaultRules, selectedRules);

        return modeRules;
    };
};
