const KartPeer = class {
    constructor(battle, id, name, UserKart) {
        this.battle = battle;
        this.helpers = new GeneralHelpers();

        // Save karts meta data
        this.id = id;
        this.name = name;
        this.causedError = false;
        this.kartClass = null;

        // Karts params
        this.maxMovementSpeed = 2;
        this.maxBodyRotationSpeed = 1;
        // this.isAlive = true;
        // this.health = 1000;

        this.diameter = 30;
        this.radius = this.diameter / 2;
        this.x = Math.round(Math.random() * (battle.stage.width - this.diameter)) + this.radius;
        this.y = Math.round(Math.random() * (battle.stage.height - this.diameter)) + this.radius;
        this.bodyRotation = Math.round(Math.random() * 360);
        this.colors = {};
        this.collisions = {
            top: false,
            left: false,
            right: false,
            bottom: false,
        };

        this.channel = {
            x: this.x,
            y: this.y,
            stage: battle.stage,
            currentTick: 0,
            totalCheckpoints: this.battle.checkpoints.length,
            heading: this.bodyRotation,
            moves: [],
            colors: {},
            kartClass: null,
            addMoves: this.addMoves,
            addColor: this.addColor,
            helpers: new GeneralHelpers(),
        };

        // Movement object structures
        this.defaultKartMovement = null;

        // Movement and queue for Kart classes
        this.kartMovement = null;

        // Set default movement values
        this.setMovementDefaults();

        // Initialize kart
        try {
            this.kart = new UserKart(this.channel);
        } catch (error) {
            this.battle.kartError(this, 'constructor', error);
        };
    };

    setMovementDefaults() {
        // Kart movement object structure
        // TODO: save active move separately. We don't want to overwrite that move.
        this.defaultKartMovement = {
            moves: {
                queue: [],
                active: 0,
                startX: null,
                startY: null,
                currentValue: null,
                targetValue: null,
            },
            setMoves: [],
        };

        // Set movement for kart
        this.kartMovement = this.defaultKartMovement;
    };

    addColor(part, color) {
        // Note: this.colors is actually this.channel.colors
        this.colors[part] = color;
    };

    addMoves(moves) {
        // Note: this.moves is actually this.channel.moves
        this.moves.push(moves);
    };

    getClass() {
        this.kartClass = this.channel.kartClass;
        delete this.channel.kartClass;
    };

    run() {
        // Reset collisions for new tick
        this.collisions = {
            top: false,
            left: false,
            right: false,
            bottom: false,
        };

        // Call karts' run method
        try {
            this.kart.run();
        } catch (error) {
            this.battle.kartError(this, 'run', error);
        };

        // Wall collisions
        this.battle.getWallCollisions(this);

        // Checkpoint collisions
        this.battle.getCheckpointCollisions(this);

        // Laser detections
        this.battle.getLaserDetections(this);

        // Save moves
        if (this.kartClass === 'Kart') {
            this.kartMovement.moves.queue = this.channel.moves;
        } else if (this.kartClass === 'AdvancedKart') {
            this.prepareNextMove();
            this.kartMovement.setMoves = this.channel.moves;
        }

        // Save colors
        this.colors = this.channel.colors;

        // Reset channel lists
        this.channel.moves = [];
        this.channel.colors = {};

        // Update channel values
        this.channel.x = this.x;
        this.channel.y = this.y;
        this.channel.currentTick = this.battle.currentTick;
        this.channel.heading = this.bodyRotation;
    };

    executeMoves() {
        let currentMoves = [];

        if (this.kartClass === 'AdvancedKart') {
            currentMoves = this.kartMovement.setMoves[0];
        }
        else if (this.kartClass === 'Kart') {
            const movement = this.kartMovement.moves;

            // Fix: Prevent active getting too high
            movement.active = movement.active >= movement.queue.length ? 0 : movement.active;
            currentMoves = [movement.queue[movement.active]];

            this.updateKartMove(currentMoves);
        };

        if(currentMoves) {
            this.updateMovementLineair(currentMoves);
        };
    };

    updateKartMove(currentMoves) {
        const movement = this.kartMovement.moves;
        let speed = 0;

        switch (currentMoves[0].name) {
            case 'ahead':
                speed = this.maxMovementSpeed;
                break;

            case 'back':
                speed = this.maxMovementSpeed * -1;
                break;

            case 'turnLeft':
                speed = this.maxBodyRotationSpeed;
                break;

            case 'turnRight':
                speed = this.maxBodyRotationSpeed * -1;
                break;
        }

        // Update progress for current move
        movement.currentValue = movement.currentValue === null ? speed : movement.currentValue + speed;
        movement.targetValue = currentMoves[0].value;

        // Nieuwe waarde niet hoger laten worden dan target value
        movement.currentValue = movement.currentValue > movement.targetValue ? movement.targetValue : movement.currentValue;

        // Determine if target value is achieved
        if (Math.abs(movement.currentValue) >= movement.targetValue) {
            this.prepareNextMove();
        }
    }

    getLastMoveOfType(type, list) {
        // TODO: Keep these lists up-to-date
        const movesForTypes = {
            y: ['setAhead', 'setBack', 'ahead', 'back'],
            rotation: ['setTurnLeft', 'setTurnRight', 'turnLeft', 'turnRight'],
        }
        const positiveValues = ['setAhead', 'ahead', 'setTurnLeft', 'turnLeft'];
        let moveObject = false;

        list.reverse().forEach(move => {
            if (movesForTypes[type].indexOf(move.name) !== -1) {
                const isPositive = positiveValues.indexOf(move.name) !== -1;
                move.isPositive = isPositive;
                moveObject = move;
            }
        });

        return moveObject;
    };

    getYMovement(moves) {
        const moveObject = this.getLastMoveOfType('y', moves);

        if (moveObject) {
            return moveObject.isPositive ? moveObject.value : 0 - moveObject.value;
        }

        return false;
    };

    getRotationMovement(moves) {
        const moveObject = this.getLastMoveOfType('rotation', moves);

        if (moveObject) {
            return moveObject.isPositive ? moveObject.value : 0 - moveObject.value;
        }

        return false;
    };

    updateMovementLineair(moves) {
        const yMovement = this.getYMovement(moves);
        const rotationMovement = this.getRotationMovement(moves);

        if (yMovement !== false) {
            const toRadians = Math.PI / 180;
            const speed = yMovement > 0 ? this.maxMovementSpeed : this.maxMovementSpeed * -1;
            const speedAxisX = Math.sin(this.bodyRotation * toRadians) * speed;
            const speedAxisY = Math.cos(this.bodyRotation * toRadians) * speed * -1;
            let newX = this.x + speedAxisX;
            let newY = this.y + speedAxisY;

            // const isColliding = this.battle.collisionService.isKartColliding(this);
            // const couldKartBeColliding = this.battle.collisionService.couldKartBeColliding(this, speed);

            // console.log(this.id, Math.round(this.x), Math.round(this.y), isColliding, couldKartBeColliding);

            const collisions = this.battle.collisionService.getKartCollisions(this);
            // const possibleCollisions = this.battle.collisionService.getPossibleKartCollisions(this, this.maxMovementSpeed)

            // console.log(this.id, Math.round(this.x), Math.round(this.y), collisions, possibleCollisions);

            collisions.forEach(collision => {
                this.collisions.left = collision.x < 0 ? true : this.collisions.left;
                this.collisions.right = collision.x > 0 ? true : this.collisions.right;
                this.collisions.top = collision.y < 0 ? true : this.collisions.top;
                this.collisions.bottom = collision.y > 0 ? true : this.collisions.top;
            });

            // Stop movement when bumping into something
            if (this.collisions.left && newX < this.x || this.collisions.right && newX > this.x || this.collisions.top && newY < this.y || this.collisions.bottom && newY > this.y) {
                newX = this.x;
                newY = this.y;
            };

            // Update kart's position
            this.x = newX;
            this.y = newY;
        };

        if (rotationMovement !== false) {
            const rotationSpeed = rotationMovement > 0 ? this.maxBodyRotationSpeed : this.maxBodyRotationSpeed * -1;
            this.bodyRotation = this.helpers.normalAbsoluteAngle(this.bodyRotation - rotationSpeed);
        };
    };

    prepareNextMove() {
        const kartMovement = this.kartMovement;

        kartMovement.moves.active = kartMovement.moves.active + 1 === this.kartMovement.moves.queue.length ? 0 : kartMovement.moves.active + 1;
        kartMovement.moves.startX = null;
        kartMovement.moves.startY = null;
        kartMovement.moves.currentValue = null;
        kartMovement.moves.targetValue = null;
        kartMovement.setMoves.moves = [];
    };
}
