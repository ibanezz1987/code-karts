const AdvancedKart = class extends Kart {
    constructor(channel) {
        super(channel);

        this.channel.kartClass = 'AdvancedKart';
        this.setMoves = [];
    };

    setAhead(distance) {
        this.setMoves.push({ name: 'setAhead', value: distance });
    };

    setBack(distance) {
        this.setMoves.push({ name: 'setBack', value: distance });
    };

    setTurnLeft(degrees) {
        this.setMoves.push({ name: 'setTurnLeft', value: degrees });
    };

    setTurnRight(degrees) {
        this.setMoves.push({ name: 'setTurnRight', value: degrees });
    };

    execute() {
        this.channel.addMoves(this.setMoves);
        this.setMoves = [];
    };
}
