# Supported methods #

See RoboCode docs:<br>
https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

---

- ahead<br>
- back<br>
- ~~doNothing~~<br>
- ~~fire~~<br>
- getStageHeight ~~getBattleFieldHeight~~<br>
- getStageWidth ~~getBattleFieldWidth~~<br>
- ~~getEnergy~~<br>
- ~~getGunCoolingRate~~<br>
- ~~getGunHeading~~<br>
- ~~getGunHeat~~<br>
- getHeading<br>
- ~~getHeight~~<br>
- ~~getNumRounds~~<br>
- ~~getNumSentries~~<br>
- ~~getOthers~~<br>
- ~~getPaintEventListener~~<br>
- ~~getRadarHeading~~<br>
- ~~getRobotRunnable~~<br>
- ~~getRoundNum~~<br>
- ~~getSentryBorderSize~~<br>
- adapted getTime (getCurrentTick)<br>
- ~~getVelocity~~<br>
- ~~getWidth~~<br>
- getX<br>
- getY<br>
- ~~onBattleEnded~~<br>
- ~~onBulletHit~~<br>
- ~~onBulletHitBullet~~<br>
- ~~onBulletMissed~~<br>
- ~~onDeath~~<br>
- ~~onHitByBullet~~<br>
- partial onHitRobot (onHitOpponent - not all event methods)<br>
- onHitWall<br>
- ~~onKeyPressed~~<br>
- ~~onKeyReleased~~<br>
- ~~onKeyTyped~~<br>
- ~~onMouseClicked~~<br>
- ~~onMouseDragged~~<br>
- ~~onMouseEntered~~<br>
- ~~onMouseExited~~<br>
- ~~onMouseMoved~~<br>
- ~~onMousePressed~~<br>
- ~~onMouseReleased~~<br>
- ~~onMouseWheelMoved~~<br>
- ~~onPaint~~<br>
- ~~onRobotDeath~~<br>
- ~~onRoundEnded~~<br>
- partial onScannedRobot (onScannedOpponent - not all event methods)<br>
- ~~onStatus~~<br>
- ~~onWin~~<br>
- ~~resume~~<br>
- ~~run~~<br>
- ~~scan~~<br>
- ~~setAdjustGunForRobotTurn~~<br>
- ~~setAdjustRadarForGunTurn~~<br>
- ~~setAdjustRadarForRobotTurn~~<br>
- ~~setAllColors~~<br>
- ~~setBodyColor~~<br>
- ~~setBulletColor~~<br>
- setColors<br>
- ~~setColors~~<br>
- ~~setDebugProperty~~<br>
- ~~setGunColor~~<br>
- ~~setRadarColor~~<br>
- ~~setScanColor~~<br>
- ~~stop~~<br>
- ~~stop~~<br>
- ~~turnGunLeft~~<br>
- ~~turnGunRight~~<br>
- turnLeft<br>
- ~~turnRadarLeft~~<br>
- ~~turnRadarRight~~<br>
- turnRight<br>

---

### ahead(double distance) ###
void<br>
Immediately moves your robot ahead (forward) by distance measured in pixels.

### back(double distance) ###
void<br>
Immediately moves your robot backward by distance measured in pixels.

### doNothing() ###
void<br>
Do nothing this turn, meaning that the robot will skip it's turn.

### fire(double power) ###
void<br>
Immediately fires a bullet.

### Bullet ###
fire<br>Bullet(double power)
Immediately fires a bullet.

### IBasicEvents ###
getB<br>asicEventListener()
This method is called by the game to notify this robot about basic robot event.

### getBattleFieldHeight() ###
doub<br>le
Returns the height of the current battlefield measured in pixels.

### getBattleFieldWidth() ###
doub<br>le
Returns the width of the current battlefield measured in pixels.

### getEnergy() ###
doub<br>le
Returns the robot's current energy.

Graphics2D
getGraphics()
Returns a graphics context used for painting graphical items for the robot.

### getGunCoolingRate() ###
doub<br>le
Returns the rate at which the gun will cool down, i.e. the amount of heat the gun heat will drop per turn.

### getGunHeading() ###
doub<br>le
Returns the direction that the robot's gun is facing, in degrees.

### getGunHeat() ###
doub<br>le
Returns the current heat of the gun.

### getHeading() ###
doub<br>le
Returns the direction that the robot's body is facing, in degrees.

### getHeight() ###
doub<br>le
Returns the height of the robot measured in pixels.

IInteractiveEvents
getInteractiveEventListener()
This method is called by the game to notify this robot about interactive events, i.e. keyboard and mouse events.

String
getName()
Returns the robot's name.

### getNumRounds() ###
int<br>
Returns the number of rounds in the current battle.

### getNumSentries() ###
int<br>
Returns how many sentry robots that are left in the current round.

### getOthers() ###
int<br>
Returns how many opponents that are left in the current round.

### getPaintEventListener() ###
IPai<br>ntEvents
This method is called by the game to notify this robot about painting events.

### getRadarHeading() ###
doub<br>le
Returns the direction that the robot's radar is facing, in degrees.

### getRobotRunnable() ###
Runn<br>able
This method is called by the game to invoke the run() method of your robot, where the program of your robot is implemented.

### getRoundNum() ###
int<br>
Returns the current round number (0 to 1) of the battle.

### getSentryBorderSize() ###
int<br>
Returns the sentry border size for a BorderSentry that defines the how far a BorderSentry is allowed to move from the border edges measured in units.

### getTime() ###
long<br>
Returns the game time of the current round, where the time is equal to the current turn in the round.

### getVelocity() ###
doub<br>le
Returns the velocity of the robot measured in pixels/turn.

### getWidth() ###
doub<br>le
Returns the width of the robot measured in pixels.

### getX() ###
doub<br>le
Returns the X position of the robot. (0,0) is at the bottom left of the battlefield.

### getY() ###
doub<br>le
Returns the Y position of the robot. (0,0) is at the bottom left of the battlefield.

### onBattleEnded(BattleEndedEvent event) ###<br>
void
This method is called after the end of the battle, even when the battle is aborted.

### onBulletHit(BulletHitEvent event) ###
void<br>
This method is called when one of your bullets hits another robot.

### onBulletHitBullet(BulletHitBulletEvent even<br>t) ###
void
This method is called when one of your bullets hits another bullet.

### onBulletMissed(BulletMissedEvent event ###<br>
void
This method is called when one of your bullets misses, i.e. hits a wall.

### onDeath(DeathEvent event) ###
void<br>
This method is called if your robot dies.

### onHitByBullet(HitByBulletEvent event) ###<br>
void
This method is called when your robot is hit by a bullet.

### onHitRobot(HitRobotEvent event) ###
void<br>
This method is called when your robot collides with another robot.

### onHitWall(HitWallEvent event) ###
void<br>
This method is called when your robot collides with a wall.

### onKeyPressed(KeyEvent e) ###
void<br>
This method is called when a key has been pressed.

### onKeyReleased(KeyEvent e) ###
void<br>
This method is called when a key has been released.

### onKeyTyped(KeyEvent e) ###
void<br>
This method is called when a key has been typed (pressed and released).

### onMouseClicked(MouseEvent e) ###
void<br>
This method is called when a mouse button has been clicked (pressed and released).

### onMouseDragged(MouseEvent e) ###
void<br>
This method is called when a mouse button has been pressed and then dragged.

### onMouseEntered(MouseEvent e) ###
void<br>
This method is called when the mouse has entered the battle view.

### onMouseExited(MouseEvent e) ###
void<br>
This method is called when the mouse has exited the battle view.

### onMouseMoved(MouseEvent e) ###
void<br>
This method is called when the mouse has been moved.

### onMousePressed(MouseEvent e) ###
void<br>
This method is called when a mouse button has been pressed.

### onMouseReleased(MouseEvent e) ###
void<br>
This method is called when a mouse button has been released.

### onMouseWheelMoved(MouseWheelEvent e) ###
void<br>
This method is called when the mouse wheel has been rotated.

### onPaint(Graphics2D g) ###
void<br>
This method is called every time the robot is painted.

### onRobotDeath(RobotDeathEvent event) ###
void<br>
This method is called when another robot dies.

### onRoundEnded(RoundEndedEvent event) ###
void<br>
This method is called after the end of a round.

### onScannedRobot(ScannedRobotEvent event)# ##<br>
void
This method is called when your robot sees another robot, i.e. when the robot's radar scan "hits" another robot.

### onStatus(StatusEvent e) ###
void<br>
This method is called every turn in a battle round in order to provide the robot status as a complete snapshot of the robot's current state at that specific time.

### onWin(WinEvent event) ###
void<br>
This method is called if your robot wins a battle.

### resume() ###
void<br>
Immediately resumes the movement you stopped by stop(), if any.

### run() ###
void<br>
The main method in every robot.

### scan() ###
void<br>
Scans for other robots.

### setAdjustGunForRobotTurn(boolean inde<br>pendent) ###
void
Sets the gun to turn independent from the robot's turn.

### setAdjustRadarForGunTurn(boolean inde<br>pendent) ###
void
Sets the radar to turn independent from the gun's turn.

### setAdjustRadarForRobotTurn(boolean inde<br>pendent) ###
void
Sets the radar to turn independent from the robot's turn.

### setAllColors(Color color) ###
void<br>
Sets all the robot's color to the same color in the same time, i.e. the color of the body, gun, radar, bullet, and scan arc.

### setBodyColor(Color color) ###
void<br>
Sets the color of the robot's body.

### setBulletColor(Color color) ###
void<br>
Sets the color of the robot's bullets.

### setColors(Color bodyColor, Color gunC<br>olor, Color radarColor) ###
void
Sets the color of the robot's body, gun, and radar in the same time.

### setColors(Color bodyColor, Color gunC<br>olor, Color radarColor, Color bulletColor, Color scanArcColor) ###
void
Sets the color of the robot's body, gun, radar, bullet, and scan arc in the same time.

### setDebugProperty(String key, String valu<br>e) ###
void
Sets the debug property with the specified key to the specified value.

### setGunColor(Color color) ###
void<br>
Sets the color of the robot's gun.

### setRadarColor(Color color) ###
void<br>
Sets the color of the robot's radar.

### setScanColor(Color color) ###
void<br>
Sets the color of the robot's scan arc.

### stop() ###
void<br>
Immediately stops all movement, and saves it for a call to resume().

### stop(boolean overwrite) ###
void<br>
Immediately stops all movement, and saves it for a call to resume().

### turnGunLeft(double degrees) ###
void<br>
Immediately turns the robot's gun to the left by degrees.

### turnGunRight(double degrees) ###
void<br>
Immediately turns the robot's gun to the right by degrees.

### turnLeft(double degrees) ###
void<br>
Immediately turns the robot's body to the left by degrees.

### turnRadarLeft(double degrees) ###
void<br>
Immediately turns the robot's radar to the left by degrees.

### turnRadarRight(double degrees) ###
void<br>
Immediately turns the robot's radar to the right by degrees.

### turnRight(double degrees) ###
void<br>
Immediately turns the robot's body to the right by degrees.
