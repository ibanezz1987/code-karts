const Kart = class {
    constructor(channel) {
        this.channel = channel;

        this.channel.kartClass = 'Kart';
    };

    ahead(distance) {
        this.channel.addMoves({ name: 'ahead', value: distance });
    };

    back(distance) {
        this.channel.addMoves({ name: 'back', value: distance });
    };

    turnLeft(degrees) {
        this.channel.addMoves({ name: 'turnLeft', value: degrees });
    };

    turnRight(degrees) {
        this.channel.addMoves({ name: 'turnRight', value: degrees });
    };

    // Sets these colors (kart parts): bodyPrimary, bodySecondary, driverPrimary, driverSecondary, framePrimary, frameSecondary
    setColors(bodyPrimary, bodySecondary, driverPrimary, driverSecondary, framePrimary, frameSecondary) {
        if (bodyPrimary) {
            this.setKartPrimaryBodyColor(bodyPrimary);
        };

        if (bodySecondary) {
            this.setKartSecondaryBodyColor(bodySecondary);
        };

        if (driverPrimary) {
            this.setDriverPrimaryColor(driverPrimary);
        };

        if (driverSecondary) {
            this.setDriverSecondaryColor(driverSecondary);
        };

        if (framePrimary) {
            this.setFramePrimaryColor(framePrimary);
        };

        if (frameSecondary) {
            this.setFrameSecondaryColor(frameSecondary);
        };
    };

    setKartBodyColor(color) {
        this.setKartPrimaryBodyColor(color);
        this.setKartSecondaryBodyColor(color);
    };

    setDriverColor(color) {
        this.setDriverPrimaryColor(color);
        this.setDriverSecondaryColor(color);
    };

    setFrameColor(color) {
        this.setFramePrimaryColor(color);
        this.setFrameSecondaryColor(color);
    };

    setFramePrimaryColor(color) {
        this.channel.addColor('framePrimary', color);
    };

    setFrameSecondaryColor(color) {
        this.channel.addColor('frameSecondary', color);
    };

    setKartPrimaryBodyColor(color) {
        this.channel.addColor('bodyPrimary', color);
    };

    setKartSecondaryBodyColor(color) {
        this.channel.addColor('bodySecondary', color);
    };

    setDriverPrimaryColor(color) {
        this.channel.addColor('driverPrimary', color);
    };

    setDriverSecondaryColor(color) {
        this.channel.addColor('driverSecondary', color);
    };

    getX() {
        return this.channel.x;
    };

    getY() {
        return this.channel.y;
    };

    getStageWidth() {
        return this.channel.stage.width;
    };
    
    getStageHeight() {
        return this.channel.stage.height;
    };

    getHeading() {
        return this.channel.heading;
    };

    getCurrentTick() {
        return this.channel.currentTick;
    };

    getTotalCheckpoints() {
        return this.channel.totalCheckpoints;
    }

    // Define event methods, so that they are always available;
    run() { };
    onHitWall() { };
    onHitOpponent() { };
    onScannedOpponent() { };
    onScannedCheckpoint() { };
    onReachCheckpoint() { };
}
