const Renderer = class {
    constructor(stageSize) {
        this.app = new PIXI.Application({
            transparent: true,
            resizeTo: window,
        });
        this.stageSize = stageSize;
        this.pixiElements = [];
        this.kartContainers = [];

        document.body.appendChild(this.app.view);

        this.cleanStage();
    };

    resetStage() {
        // Remove all pixi elements from the stage and memory.
        this.pixiElements.forEach(element => {
            element.destroy(true);
        });

        this.pixiElements = [];
    };

    cleanStage() {
        // Clear kart containers list
        this.kartContainers = [];

        this.drawStageBorders();
    };

    // TODO: Figure out if making the stage fit the screen is a good idea.
    // TODO: Center stage?
    // TODO: Zooming?
    drawStageBorders() {
        const graphics = new PIXI.Graphics();
        const borderWidth = 30;

        this.pixiElements.push(graphics);
        graphics.beginFill(0x000000);

        // draw a rectangle
        graphics.drawRect(this.stageSize.width, 0, borderWidth, this.stageSize.height + borderWidth);
        graphics.drawRect(0, this.stageSize.height, this.stageSize.width + borderWidth, borderWidth);

        this.app.stage.addChild(graphics);
    };

    drawTrack(points) {
        const graphics = new PIXI.Graphics();

        this.pixiElements.push(graphics);

        for (var i = 0; i < points.length; i++) {
            var point = points[i];

            this.drawCircle(point, point.radius, graphics);
        }

        this.app.stage.addChild(graphics);
    };

    drawCircle(coordinates, size, graphicsParent) {
        const x = coordinates.x;
        const y = coordinates.y;
        let graphics = null;

        if (graphicsParent) {
            graphics = graphicsParent;
        }
        else {
            graphics = new PIXI.Graphics();
            this.pixiElements.push(graphics);
        };

        graphics.lineStyle(0);
        graphics.beginFill(0x555555, 1);
        graphics.drawCircle(x, y, size);
        graphics.endFill();
    };

    updateKart(kartPeer) {
        let container = this.kartContainers.length > kartPeer.id ? this.kartContainers[kartPeer.id] : null;
        let colors = kartPeer.colors;

        if (!container) {
            container = new PIXI.Container();
            this.kartContainers[kartPeer.id] = container;

            const wheelsTexture = PIXI.Texture.from('img/kart-small/wheels.png');
            const wheelsSprite = new PIXI.Sprite(wheelsTexture);
            const engineTexture = PIXI.Texture.from('img/kart-small/engine.png');
            const engineSprite = new PIXI.Sprite(engineTexture);
            const bodyPrimaryTexture = PIXI.Texture.from('img/kart-small/body-primary.png');
            const bodyPrimarySprite = new PIXI.Sprite(bodyPrimaryTexture);
            const bodySecondaryTexture = PIXI.Texture.from('img/kart-small/body-secondary.png');
            const bodySecondarySprite = new PIXI.Sprite(bodySecondaryTexture);
            const driverPrimaryTexture = PIXI.Texture.from('img/kart-small/driver-primary.png');
            const driverPrimarySprite = new PIXI.Sprite(driverPrimaryTexture);
            const driverSecondaryTexture = PIXI.Texture.from('img/kart-small/driver-secondary.png');
            const driverSecondarySprite = new PIXI.Sprite(driverSecondaryTexture);
            const framePrimaryTexture = PIXI.Texture.from('img/kart-small/frame-primary.png');
            const framePrimarySprite = new PIXI.Sprite(framePrimaryTexture);
            const frameSecondaryTexture = PIXI.Texture.from('img/kart-small/frame-secondary.png');
            const frameSecondarySprite = new PIXI.Sprite(frameSecondaryTexture);

            const laser = new PIXI.Graphics();
            laser.lineStyle(0);
            laser.beginFill(0xff0000, 1);
            laser.drawRect(0, 0, 1, this.stageSize.width + this.stageSize.height)
            laser.endFill();

            this.pixiElements.push(wheelsTexture);
            this.pixiElements.push(wheelsSprite);
            this.pixiElements.push(engineTexture);
            this.pixiElements.push(engineSprite);
            this.pixiElements.push(bodyPrimaryTexture);
            this.pixiElements.push(bodyPrimarySprite);
            this.pixiElements.push(bodySecondaryTexture);
            this.pixiElements.push(bodySecondarySprite);
            this.pixiElements.push(driverPrimaryTexture);
            this.pixiElements.push(driverPrimarySprite);
            this.pixiElements.push(driverSecondaryTexture);
            this.pixiElements.push(driverSecondarySprite);
            this.pixiElements.push(framePrimaryTexture);
            this.pixiElements.push(framePrimarySprite);
            this.pixiElements.push(frameSecondaryTexture);
            this.pixiElements.push(frameSecondarySprite);

            this.pixiElements.push(laser);

            wheelsSprite.anchor.set(0.5);
            wheelsSprite.x = 0;
            wheelsSprite.y = 0;
            engineSprite.anchor.set(0.5);
            engineSprite.x = 0;
            engineSprite.y = 0;
            bodyPrimarySprite.anchor.set(0.5);
            bodyPrimarySprite.x = 0;
            bodyPrimarySprite.y = 0;
            bodySecondarySprite.anchor.set(0.5);
            bodySecondarySprite.x = 0;
            bodySecondarySprite.y = 0;
            driverPrimarySprite.anchor.set(0.5);
            driverPrimarySprite.x = 0;
            driverPrimarySprite.y = 0;
            driverSecondarySprite.anchor.set(0.5);
            driverSecondarySprite.x = 0;
            driverSecondarySprite.y = 0;
            framePrimarySprite.anchor.set(0.5);
            framePrimarySprite.x = 0;
            framePrimarySprite.y = 0;
            frameSecondarySprite.anchor.set(0.5);
            frameSecondarySprite.x = 0;
            frameSecondarySprite.y = 0;

            laser.y = - this.stageSize.width - this.stageSize.height;

            if (colors.bodyPrimary) {
                bodyPrimarySprite.tint = colors.bodyPrimary;
            }
            if (colors.bodySecondary) {
                bodySecondarySprite.tint = colors.bodySecondary;
            }
            if (colors.driverPrimary) {
                driverPrimarySprite.tint = colors.driverPrimary;
            }
            if (colors.driverSecondary) {
                driverSecondarySprite.tint = colors.driverSecondary;
            }
            if (colors.framePrimary) {
                framePrimarySprite.tint = colors.framePrimary;
            }
            if (colors.frameSecondary) {
                frameSecondarySprite.tint = colors.frameSecondary;
            }

            // The folloring order is chosen with care
            container.addChild(frameSecondarySprite);
            container.addChild(framePrimarySprite);
            container.addChild(wheelsSprite);
            container.addChild(bodySecondarySprite);
            container.addChild(engineSprite);
            container.addChild(driverSecondarySprite);
            container.addChild(bodyPrimarySprite);
            container.addChild(driverPrimarySprite);

            container.addChild(laser);

            this.app.stage.addChild(container);
        }

        container.x = kartPeer.x;
        container.y = kartPeer.y;
        container.angle = kartPeer.bodyRotation;
    };
}
