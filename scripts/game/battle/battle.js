const Battle = class {
    constructor(battleSettings, renderer, track) {
        this.battleSettings = battleSettings;
        this.renderer = renderer;
        this.track = track;
        this.checkpoints = [];
        this.kartPeers = [];

        this.isActive = true;
        this.isPaused = this.battleSettings.getValue('isPaused');
        this.currentTick = 0;
        this.stage = this.battleSettings.getValue('stage');
        this.selectedKarts = this.battleSettings.getValue('selectedKarts');
        this.defaultFrameRate = this.battleSettings.getValue('frameRate');
        this.frameRate = this.defaultFrameRate;
        this.validator = new Validator();
        this.collisionService = new CollisionService(this);

        this.getTrack();
        this.getKarts();

        // Kart classes en events ophalen voor starten battle
        this.kartPeers.forEach(kartPeer => {
            kartPeer.getClass();
        });
    };

    quit() {
        // TODO: Check if current battle class is removed from memory
        this.isActive = false;
    };

    getTrack() {
        this.checkpoints = this.track.getTrack();
        this.renderer.drawTrack(this.checkpoints);
    };

    getKarts() {
        // TODO: Load karts from file / db at some point
        let selectedKartIds = this.selectedKarts;
        let kartIndex = 0;

        selectedKartIds.forEach(kartId => {
            const kart = this.battleSettings.karts[kartId],
                validation = this.validator.validate(kart.class);

            if (validation.isValid) {
                // TODO: use object to pass params in a nicer looking way
                this.kartPeers.push(new KartPeer(this, kartIndex, kart.name, kart.class));
                kartIndex++;
            }
            else {
                this.kartInvalid(kart.name, validation);
            }
        });
    };

    gameLoop() {
        this.frameRate = this.battleSettings.frameRate;

        setTimeout(() => {
            if (this.isActive && !this.isPaused) {
                this.gameTick();
            };
        }, 1000 / this.frameRate);
    };

    gameTick() {
        this.kartPeers.forEach(kartPeer => {
            if (!kartPeer.causedError) {
                // Find run moves at the start of each tick
                kartPeer.run();

                // Update karts
                kartPeer.executeMoves();

                // Update renderer
                this.renderer.updateKart(kartPeer);
            }
        });

        this.currentTick++;

        this.gameLoop();
    };

    togglePaused() {
        this.isPaused = !this.isPaused;

        // Restart gameloop
        if (!this.isPaused) {
            this.gameLoop();
        }

        return this.isPaused;
    };

    kartError(kartPeer, step, error) {
        console.group();
        console.info(kartPeer.name + ' caused the following error during ' + step + '():');
        console.warn(error);
        console.groupEnd();

        kartPeer.causedError = true;
    };

    kartInvalid(name, validation) {
        console.warn(name + ' didn\'t pass validation. Validation found "' + validation.invalidValue + '"');
    }

    getWallCollisions(kartPeer) {
        this.collisionService.getWallCollisions(kartPeer);
    };

    getLaserDetections(kartPeer) {
        this.collisionService.getLaserDetections(kartPeer);
    };

    getCheckpointCollisions(kartPeer) {
        this.collisionService.getCheckpointCollisions(kartPeer);
    };
}
