const CollisionService = class {
    constructor(battle) {
        this.helpers = new GeneralHelpers();
        this.spacialHelpers = new SpacialHelpers();
        this.kartHelpers = new KartHelpers(battle.kartPeers);
        this.battle = battle;
        this.stage = battle.stage;
    };

    // TODO: search for optimal way to detect collisions
    getKartCollisions(currentKart) {
        const otherKarts = this.kartHelpers.getOtherKarts(currentKart);
        let collisions = [];

        otherKarts.forEach(kart => {
            if (this.spacialHelpers.circleIntersect(currentKart.x, currentKart.y, currentKart.radius, kart.x, kart.y, kart.radius)) {
                // https://spicyyoghurt.com/tutorials/html5-javascript-game-development/collision-detection-physics
                var vCollision = { x: kart.x - currentKart.x, y: kart.y - currentKart.y };
                var vCollisionNorm = { x: vCollision.x / currentKart.diameter, y: vCollision.y / currentKart.diameter };

                collisions.push(vCollisionNorm);
            };
        });

        // TODO: improve code
        if (collisions.length) {
            try {
                currentKart.kart.onHitOpponent({
                    // TODO: add additional information
                    // getBearing: function () {
                    //     return bearing;
                    // },
                });
            } catch (error) {
                currentKart.battle.kartError(currentKart, 'onHitOpponent', error);
            };
        }


        return collisions;

        // // TODO give this back
        // const collisions = {
        //     left: 0, // distance
        //     right: -1, // no collision
        //     top: -1,
        //     bottom: -1,
        // }
    };

    // getPossibleKartCollisions(currentKart, speed) {
    //     const otherKarts = this.kartHelpers.getOtherKarts(currentKart);
    //     let collisions = [];

    //     otherKarts.forEach(kart => {
    //         if (this.spacialHelpers.circleIntersect(currentKart.x, currentKart.y, currentKart.radius + speed * 2, kart.x, kart.y, kart.radius + speed * 2)) {
    //             // https://spicyyoghurt.com/tutorials/html5-javascript-game-development/collision-detection-physics
    //             var vCollision = { x: kart.x - currentKart.x, y: kart.y - currentKart.y };
    //             var distance = Math.sqrt((kart.x - currentKart.x) * (kart.x - currentKart.x) + (kart.y - currentKart.y) * (kart.y - currentKart.y));
    //             var vCollisionNorm = { x: vCollision.x / distance, y: vCollision.y / distance };

    //             collisions.push(vCollisionNorm);
    //         };
    //     });

    //     return collisions;

    //     // // TODO give this back
    //     // const collisions = {
    //     //     left: 5, // distance
    //     //     right: -1, // no collision
    //     //     top: -1,
    //     //     bottom: -1,
    //     // }
    // };

    getWallCollisions(kartPeer) {
        let hitUpperBoundary = kartPeer.y <= kartPeer.radius;
        let hitLeftBoundary = kartPeer.x <= kartPeer.radius;
        let hitBottomBoundary = kartPeer.y >= this.stage.height - kartPeer.radius;
        let hitRightBoundary = kartPeer.x >= this.stage.width - kartPeer.radius;

        if (hitUpperBoundary || hitLeftBoundary || hitBottomBoundary || hitRightBoundary) {
            // Send relative angle to player
            let boundaryDirection = 0;
            boundaryDirection = hitLeftBoundary ? 270 : boundaryDirection;
            boundaryDirection = hitBottomBoundary ? 180 : boundaryDirection;
            boundaryDirection = hitRightBoundary ? 90 : boundaryDirection;

            // Add new collisions to existing ones
            kartPeer.collisions.left = hitLeftBoundary ? true : kartPeer.collisions.left;
            kartPeer.collisions.right = hitRightBoundary ? true : kartPeer.collisions.right;
            kartPeer.collisions.top = hitUpperBoundary ? true : kartPeer.collisions.top;
            kartPeer.collisions.bottom = hitBottomBoundary ? true : kartPeer.collisions.bottom;

            let bearing = this.helpers.normalRelativeAngle(boundaryDirection - kartPeer.bodyRotation);

            try {
                kartPeer.kart.onHitWall({
                    getBearing: function () {
                        return bearing;
                    },
                });
            } catch (error) {
                kartPeer.battle.kartError(kartPeer, 'onHitWall', error);
            };
        };
    };

    // TODO: search for optimal way to detect collisions
    getCheckpointCollisions(currentKart) {
        const checkpoints = this.battle.checkpoints;
        let collisions = [];

        checkpoints.forEach(checkpoint => {
            if (this.spacialHelpers.circleIntersect(currentKart.x, currentKart.y, currentKart.radius, checkpoint.x, checkpoint.y, checkpoint.radius)) {
                collisions.push(checkpoint);
            };
        });

        // TODO: improve code
        if (collisions.length) {
            try {
                currentKart.kart.onReachCheckpoint({
                    // TODO: getting only the first is not optimal
                    index: collisions[0].index
                });
            } catch (error) {
                currentKart.battle.kartError(currentKart, 'onReachCheckpoint', error);
            };
        };
    };

    getLaserDetections(kartPeer) {
        const otherKarts = this.kartHelpers.getOtherKarts(kartPeer);
        const distance = this.stage.width + this.stage.height; // TODO: get a better max distance from diagonal
        const radians = this.helpers.degreesToRadians(kartPeer.bodyRotation);
        const laserPoint = {
            x: kartPeer.x + Math.sin(radians) * distance,
            y: kartPeer.y + (Math.cos(radians) * distance) * -1,
        };
        let kartDetections = [];
        let checkpointDetections = [];
        
        // Detect Karts
        otherKarts.forEach(kart => {
            if (this.spacialHelpers.isLineIntersectingCircle(kartPeer.x, kartPeer.y, laserPoint.x, laserPoint.y, kart.x, kart.y, kart.radius)) {
                kartDetections.push(kart);
            };
        });
        
        // Detect Checkpoints
        this.battle.checkpoints.forEach(checkpoint => {
            if (this.spacialHelpers.isLineIntersectingCircle(kartPeer.x, kartPeer.y, laserPoint.x, laserPoint.y, checkpoint.x, checkpoint.y, checkpoint.radius)) {
                // Don't scan checkpoints you're standing on
                if(!this.spacialHelpers.circleIntersect(kartPeer.x, kartPeer.y, kartPeer.radius, checkpoint.x, checkpoint.y, checkpoint.radius)) {
                    checkpointDetections.push(checkpoint);
                };
            };
        });

        // TODO: improve code
        if (kartDetections.length) {
            try {
                kartPeer.kart.onScannedOpponent({
                    // TODO: add additional information
                });
            } catch (error) {
                kartPeer.battle.kartError(kartPeer, 'onScannedOpponent', error);
            };
        };

        // TODO: improve code
        if (checkpointDetections.length) {
            try {
                kartPeer.kart.onScannedCheckpoint({
                    // TODO: getting only the first is not optimal
                    index: checkpointDetections[0].index,
                    // TODO: add additional information
                });
            } catch (error) {
                kartPeer.battle.kartError(kartPeer, 'onScannedCheckpoint', error);
            };
        };
    };
}
