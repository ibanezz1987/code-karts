//
// Game object
//

const Game = class {
    constructor() {
        this.battleSettings = new BattleSettings();
        this.stage = this.battleSettings.getValue('stage');
        this.renderer = new Renderer(this.stage);
        this.track = new Track();
        this.ui = new UI(this, this.battleSettings);
        this.trackEditor = null;
        this.battle = null;

        this.startNewBattle();
    };

    requestEmptyStage() {
        // TODO: Add nice alert to set this variable
        this.continueAction = true;

        if (this.continueAction) {
            this.stopPreviousBattle();
            this.stopTrackEditor();
            this.cleanStage();
            this.ui.toggleMenu();
        };
    };

    requestNewBattle() {
        // TODO: Add nice alert to set this variable
        this.continueAction = true;

        if (this.continueAction) {
            this.stopPreviousBattle();
            this.stopTrackEditor();
            this.cleanStage();
            this.ui.toggleMenu();
            this.startNewBattle();
        };
    };

    stopPreviousBattle() {
        if(this.battle !== null) {
            this.battle.quit();
        };
    };

    stopTrackEditor() {
        if(this.trackEditor !== null) {
            this.trackEditor.quit();
        };
    };

    cleanStage() {
        this.renderer.resetStage();
        this.renderer.cleanStage(this.stage);
        this.ui.setPauseButtonActive();
    };

    startNewBattle() {
        this.battle = new Battle(this.battleSettings, this.renderer, this.track);
        this.battle.gameLoop();
    };
};
