const UI = class {
    constructor(game, battleSettings) {
        this.game = game;
        this.battleSettings = battleSettings;
        this.tools = new Tools();
        this.bello = this.tools.bello;

        this.menu = {};
        this.currentGame = {};
        this.newGame = {};
        this.trackEditor = {};

        this.initializeMenu();
        this.addMenuEvents();

        this.initializeCurrentGameMenu();
        this.setCurrentGameValues();

        this.initializeNewGameMenu();
        this.setNewGameValues();

        this.initializeTrackEditorMenu();
        this.setTrackEditorValues();
    };

    initializeMenu() {
        this.menu.wrapper = this.bello.go('[data-menu]');
        this.menu.toggler = this.bello.go('[data-toggler]');
        this.menu.links = this.bello.gogo('[data-menulinks] button');
        this.menu.submenus = this.bello.gogo('[data-submenu]');

        this.menu.toggler.classList.remove('hidden');
    };

    addMenuEvents() {
        // Open menu
        this.menu.toggler.addEventListener('click', function (e) {
            this.toggleMenu();
        }.bind(this), false);

        // Open submenu
        this.menu.links.forEach(activeLink => {
            activeLink.addEventListener('click', function (e) {
                this.openSubmenu(activeLink);
            }.bind(this), false);
        });
    };

    toggleMenu() {
        this.menu.wrapper.classList.toggle('is-active');
        this.menu.toggler.classList.toggle('is-active');

        this.bello.go('canvas').classList.add('transition');
        this.bello.go('canvas').classList.toggle('glass');
    };

    openSubmenu(activeLink) {
        // Update active link
        this.menu.links.forEach(link => {
            if (link === activeLink) {
                activeLink.classList.add('is-active');
            }
            else {
                link.classList.remove('is-active');
            }
        });

        // Make submenu visible
        const submenuName = activeLink.dataset.button;

        this.menu.submenus.forEach(submenu => {
            const activeSubMenu = this.bello.go('[data-submenu="' + submenuName + '"]');

            if (submenu === activeSubMenu) {
                submenu.classList.add('is-active');
            }
            else {
                submenu.classList.remove('is-active');
            }
        });
    };

    initializeNewGameMenu() {
        this.newGame.submit = this.bello.go('[data-newgame="submit"]');
        this.newGame.stageWidth = this.bello.go('[data-newgame="width"]');
        this.newGame.stageHeight = this.bello.go('[data-newgame="height"]');

        this.newGame.stageWidth.value = this.battleSettings.stage.width;
        this.newGame.stageHeight.value = this.battleSettings.stage.height;
    };

    setNewGameValues() {
        this.newGame.submit.addEventListener('click', function (e) {
            this.battleSettings.stage.width = Number(this.newGame.stageWidth.value);
            this.battleSettings.stage.height = Number(this.newGame.stageHeight.value);

            this.game.requestNewBattle();
        }.bind(this), false);
    };

    initializeCurrentGameMenu() {
        this.currentGame.playPause = this.bello.go('[data-currentgame="play-pause"]');
        this.currentGame.frameRate = this.bello.go('[data-currentgame="framerate"]');

        this.currentGame.frameRate.value = this.battleSettings.frameRate;
    };

    setCurrentGameValues() {
        this.currentGame.playPause.addEventListener('click', function (e) {
            this.togglePaused();
        }.bind(this), false);

        this.currentGame.frameRate.addEventListener('change', function (e) {
            this.battleSettings.frameRate = this.currentGame.frameRate.value;
        }.bind(this), false);
    };

    togglePaused() {
        const isPaused = this.game.battle.togglePaused();

        if (isPaused) {
            this.currentGame.playPause.classList.remove('is-active');
        }
        else {
            this.currentGame.playPause.classList.add('is-active');
        }
    };

    setPauseButtonActive() {
        this.currentGame.playPause.classList.add('is-active');
    };

    initializeTrackEditorMenu() {
        this.trackEditor.new = this.bello.go('[data-trackeditor="new"]');
        this.trackEditor.save = this.bello.go('[data-trackeditor="save"]');
    };

    setTrackEditorValues() {
        this.trackEditor.new.addEventListener('click', function (e) {
            this.game.requestEmptyStage();
            
            if(this.game.trackEditor === null) {
                this.game.trackEditor = new TrackEditor(this.game.renderer, this.bello.go('canvas'));
            } else {
                this.game.trackEditor.start();
            };
        }.bind(this), false);

        this.trackEditor.save.addEventListener('click', function (e) {
            if(this.game.trackEditor) {
                const track = this.game.trackEditor.getTrack();
                const trackStringified = JSON.stringify(track);

                this.game.track.setTrack(track);
    
                // TODO: Save TrackEditor track
                console.log(trackStringified);
            };
        }.bind(this), false);
    };
};
